import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://fastscout-staging.fastmodelsports.com/')

WebUI.setText(findTestObject('Sign In Page (Staging)/Email Field'), 'qaautomation2@staging.com')

WebUI.setEncryptedText(findTestObject('Sign In Page (Staging)/Password Field'), 'YzGUhxdT33ALtOhrSF0KEQ==')

WebUI.click(findTestObject('Sign In Page (Staging)/Sign In Button'))

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.verifyElementNotPresent(findTestObject('Dashboard Page (Folder)/Advanced Stats table (Dashboard)'), 0)

WebUI.verifyElementNotPresent(findTestObject('Dashboard Page (Folder)/ADVANCED boxscore button'), 0)

WebUI.verifyElementNotPresent(findTestObject('Dashboard Page (Folder)/PER 40 boxscore button'), 0)

WebUI.verifyElementNotPresent(findTestObject('Top Nav/PLAYS tab'), 0)

WebUI.click(findTestObject('Top Nav/VIDEOS tab'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video Object'), '/Users/jefffeldman/Desktop/Vids/Press.mp4')

WebUI.verifyElementVisible(findTestObject('VIDEOS page (Folder)/First Row on VIDEOS page'))

WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

WebUI.mouseOver(findTestObject('Scouts - Opponent Scouts/Opponent Scouts 1st Row'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/Scout Content Menu (Opponent Scouts)'))

WebUI.verifyElementNotPresent(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Presenter Mode (Scout Context Menu)'), 
    0)

WebUI.verifyElementNotPresent(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Mobile Access (Scout Context Menu)'), 
    0)

WebUI.verifyElementNotPresent(findTestObject('Scouts - Opponent Scouts/Scout Context Menu/Mobile Usage'), 0)

WebUI.click(findTestObject('Scouts - Opponent Scouts/Open first opponent scout row'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Individual Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Ind Boxscore OPTIONS tab (for TC9)'))

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/USG Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/Plus-Minus'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/eFG Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/TS Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/OREB Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/DREB Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/AST Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/TO Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/STL Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementAttributeValue(findTestObject('ScoutBuilder/Tiles/STATS tab/BLK Perct'), 'data-enabled', 'false', 0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Cumulative Boxscore Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Advanced Stats Tile'), 0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile'), 0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Lineup Stats Tile'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Personnel STATS tab (TC9)'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/3 Point Shooters Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/FT Shooters Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Rebounds (Folder)/Rebounds Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Ball Control Tile (Folder)/Ball Control Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Defense Tile (Folder)/Defense Tile'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/CANCEL button (Tile Builder)'))

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'), 0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Tiles Menu_Plays'), 0)

WebUI.verifyElementNotPresent(findTestObject('ScoutBuilder/Presenter Mode (ScoutBuilder)'), 0)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Text Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Add Videos (Text Tile)'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video Object'), '/Users/jefffeldman/Desktop/Vids/Press.mp4')

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/DONE button'))

WebUI.closeBrowser()

