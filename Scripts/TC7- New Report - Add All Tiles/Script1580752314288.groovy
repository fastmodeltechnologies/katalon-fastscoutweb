import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'common.Actions.signInUser1'()

WebUI.waitForElementVisible(findTestObject('Top Nav/SCOUTS tab'), 60)

WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

WebUI.click(findTestObject('SCOUTS sub-nav/SELF SCOUTS tab'))

WebUI.click(findTestObject('Scouts - Self Scouts/NEW SCOUTING REPORT'))

WebUI.setText(findTestObject('NEW SCOUTING REPORT modal/SCOUTING REPORT NAME field'), 'TC7')

WebUI.click(findTestObject('NEW SCOUTING REPORT modal/Blank Scouting Report'))

WebUI.click(findTestObject('NEW SCOUTING REPORT modal/CREATE button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Section Header Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Spacer Tile (Folder)/Spacer Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Text Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/FastScout Factors Tile'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Chart Tile (Folder)/FastScout Factors Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 2'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Points Per Period Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Points Per Period Tile (Presense Of)'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Pace Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Individual Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Individual Boxscore Game 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Individual Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 3'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Cumulative Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Cumulative Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Stat Comparison Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 4'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Stat Splits Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Splits Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Advanced Stats Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Advanced Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Clutch Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 5'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Lineup Stats Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Lineup Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel PLAYERS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Personnel Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Top Scorers Tile'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Recent Games Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Recent Games Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/Depth Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Depth Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Custom Table Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Add Page button'))

WebUI.click(findTestObject('ScoutBuilder/Pagination/Page 6'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Team Shot Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Player Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Tiles Menu_Plays'))

WebUI.delay(3)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/DONE button'))

CustomKeywords.'common.Actions.archiveSelfScout'()

WebUI.click(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

CustomKeywords.'common.Actions.deleteScout'()

WebUI.closeBrowser()

