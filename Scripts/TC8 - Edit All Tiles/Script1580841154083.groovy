import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'common.Actions.signIn'()

CustomKeywords.'common.Actions.createOpponentScoutBlankTemplate'()

CustomKeywords.'common.Actions.addSectionHeaderTile'()

CustomKeywords.'common.Actions.editSectionHeaderTile'()

WebUI.click(findTestObject('ScoutBuilder/Tiles/Section Header Tile (Folder)/Section Header background color (1)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

CustomKeywords.'common.Actions.deleteSectionHeaderTile'()

WebUI.click(findTestObject('ScoutBuilder/Tiles/Spacer Tile (Folder)/Spacer Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Spacer Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Text Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Text Tile (Folder)/Add Videos (Text Tile)'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video Object'), '/Users/jefffeldman/Desktop/Vids/Press.mp4')

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Video 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Text Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Tile'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.uploadFile(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Image Object'), '/Users/jefffeldman/Desktop/testimage.png')

WebUI.delay(2)

WebUI.refresh()

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Image Tile (Folder)/Uploaded Image (Presence Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Image Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

CustomKeywords.'common.Actions.addFastScoutFactorsTile'()

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/OPTIONS tab (FastScout Factors tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/STATS tab (FastScout Factors tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/VIDEOS tab (FastScout Factors tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/OPTIONS tab (FastScout Factors tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/League Rankings button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/STATS tab (FastScout Factors tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/STATS tab (FastScout Factors)/FTRate Checkbox (FastScout Factors)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Tile (Folder)/STATS tab (FastScout Factors)/AST Pct Checkbox (FastScout Factors)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Chart Tile (Folder)/FastScout Factors Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/Conference split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/FastScout Factors Chart OPTIONS'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Chart Tile (Folder)/FF Chart Dropdown'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/FastScout Factors Chart Tile (Folder)/FF Chart Dropdown TSPerc'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/FastScout Factors Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Points Per Period Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Points Per Period Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Dropdown (Tile Editor)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/My Team (Team Dropdown) Tile Editor'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/PPP Overtime Checkbox'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Points Per Period Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Pace Tile (Folder)/Team Pace Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Pace OPTIONS'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Pace Tile (Folder)/My Conference checkbox (Team Pace)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/League Rankings button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Pace Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/Home split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Pace Chart OPTIONS'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Pace Chart Tile (Folder)/Opponent Conference checkbox (Team Pace Chart)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Pace Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Individual Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Individual Boxscore Game 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Individual Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Ind Boxscore OPTIONS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/USG Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Plus-Minus'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/REB'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'), 'myStat')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Individual Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Cumulative Boxscore Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Cumulative Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/First listed game'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Cum Boxscore STATS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/eFG Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/3P-R'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/FTM-A'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/STL'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'), 'myStat')

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/USG Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/PTS Column Order'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Cum Boxscore PLAYERS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Cumulative Boxscore Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Stat Comparison Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Stat Comparison VIDEOS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/ADD VIDEO button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/From Synergy'))

WebUI.setText(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Username'), 'sachin@fastmodeltechnologies.com')

WebUI.setEncryptedText(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Password'), 'YzGUhxdT33BEbO8XdJTy9g==')

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Login button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/Last 5 split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Stat Comparison VIDEOS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/ADD VIDEO button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/From Synergy'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/First Synergy Edit'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/First Synergy Clip'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/DONE button in Synergy Clips modal'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/ADD CLIPS button in Synergy Edits modal'))

WebUI.delay(2)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/VIDEOS (ScoutBuilder)/Synergy Video (Presence Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Comparison Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Stat Splits Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Splits Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/All Games offensive split (Team Stat Splits tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/All Games defensive split (Team Stat Splits tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/Add Custom Split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/All button in NEW CUSTOM SPLIT modal'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/SPLIT NAME text box'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/SPLIT NAME text box'), 'MySplit')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/ADD Custom Split button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Stat Splits STATS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/PTS'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/PACE (on Team Stat Splits)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'), 'myStat')

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Whole Numbers'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Stat Splits SAMPLE tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Stat Splits Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Team Advanced Stats Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Advanced Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Team Advanced Stats OPTIONS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/PW'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/PL'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/League Rankings button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Advanced Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Clutch Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile (Folder)/Clutch Stats Time Remaining'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile (Folder)/Clutch Stats Time Remaining'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile (Folder)/Clutch Stats Time Remaining'), 
    '3')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile (Folder)/Clutch Stats Score Within'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile (Folder)/Clutch Stats Score Within'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Clutch Stats Tile (Folder)/Clutch Stats Score Within'), 
    '3')

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'), 'myStat')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Clutch Stats PLAYERS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Clutch Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Lineup Stats Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Lineup Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Size option (3)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/TO Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/PACE'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'), 'myStat')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Minimum Minutes'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Minimum Minutes'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Minimum Minutes'), '30')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Lineups Displayed (Count)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Lineups Displayed (Count)'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Lineups Displayed (Count)'), 
    Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Lineup Stats Lineups Displayed (Count)'), 
    '5')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Lineup Stats Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Tiles Menu_Personnel'))

'delete after updating subsequent step with ID object'
WebUI.delay(1)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel PLAYERS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/Personnel Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Personnel Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Personnel Tiles/DEFAULT INFO - Position'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Personnel GAMES tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Personnel Conference Split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Personnel STATS tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/USG Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/REB'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/STATS tab/Custom Stat Text Field'), 'myStat')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Personnel Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Top Scorers Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Minimum FGA Field (Top Scorers)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Top Scorers Tile (Folder)/Minimum FGA Field (Top Scorers)'), 
    '5')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/3 Point Shooters Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Minimum Leader Rows'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Minimum Leader Rows'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Minimum Leader Rows'), '3')

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/3PM-A Column Order'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/Minimum 3PA Field (3PT Shooters)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/Minimum 3PA Field (3PT Shooters)'), 
    '5')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/3 Point Shooters Tile (Folder)/3P-R (3PT Shooters Tile)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/FT Shooters Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/Minimum FTA Field (FT Shooters)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/FT Shooters Tile (Folder)/Minimum FTA Field (FT Shooters)'), 
    '2')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/FT Shooters Sort By FT Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/FT Shooters Sort By FT Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Rebounds (Folder)/Rebounds Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Rebound Tile GAMES tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/Last 5 split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Ball Control Tile (Folder)/Ball Control Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/STATS tab/Per 40 Stat Type'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Tiles Menu_Leaders'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Defense Tile (Folder)/Defense Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Defense Tile Header Textbox'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Defense Tile Header Textbox (Input)'), 
    ' edit')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Leaders Tiles/Defense Tile (Folder)/BLK column header (on tile)'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Leader Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Recent Games Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Recent Games Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Show Last X Games (Recent Games)'))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Show Last X Games (Recent Games)'), Keys.chord(
        Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('ScoutBuilder/Tiles/Recent Games Tile (Folder)/Show Last X Games (Recent Games)'), '8')

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Recent Games DATE column'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Recent Games Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/Depth Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Depth Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/First Name (Depth Chart)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Depth Chart Tile (Folder)/First Initial'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Depth Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Tile'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Custom Table Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Settings button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table Add Row'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Custom Table Tile (Folder)/Custom Table close settings'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Custom Table Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Team Shot Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Zone Shot Chart Type'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Shot Chart Show FG Perct'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Team Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Tiles Menu_Shot Charts'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Player 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.delay(2)

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Player Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Player Shot Chart Player 2'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shot Chart Tiles/Make-Miss Shot Chart Type'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Objects I know I have to replace/Player Shot Chart GAMES tab'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/GAMES tab/Last 5 split'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Player Shot Chart Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Tiles Menu_Plays'))

WebUI.delay(3)

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 1'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Settings'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Plays Tiles/Plays Tile 2'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/ADD-SAVE TILE button'))

WebUI.mouseOver(findTestObject('ScoutBuilder/Tiles/Tiles on Page (Validation)/Play Tile (Presense Of)'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Tile Ellipsis Button'))

WebUI.click(findTestObject('ScoutBuilder/Tiles/Shared Tile Objects/Delete Tile'))

WebUI.click(findTestObject('ScoutBuilder/DONE button'))

CustomKeywords.'common.Actions.archiveOpponentScout'()

WebUI.click(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

CustomKeywords.'common.Actions.deleteScout'()

WebUI.closeBrowser()

