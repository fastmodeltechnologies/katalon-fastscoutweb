import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'common.Actions.signInUser1'()

WebUI.click(findTestObject('Top Nav/SCOUTS tab'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/NEW SCOUTING REPORT button'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/First Upcoming Game'))

WebUI.click(findTestObject('Scouts - Opponent Scouts/Saved Template Dropdown'))

WebUI.sendKeys(findTestObject('Scouts - Opponent Scouts/Saved Template Dropdown'), '6')

WebUI.sendKeys(findTestObject('Scouts - Opponent Scouts/Saved Template Dropdown'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('NEW SCOUTING REPORT modal/CREATE button'))

WebUI.delay(10)

WebUI.verifyElementVisible(findTestObject('ScoutBuilder/Tiles/Team Stats Tiles/Tiles Menu_Team Stats'))

WebUI.click(findTestObject('ScoutBuilder/DONE button'))

CustomKeywords.'common.Actions.archiveOpponentScout'()

WebUI.click(findTestObject('SCOUTS sub-nav/ARCHIVED tab'))

CustomKeywords.'common.Actions.deleteScout'()

WebUI.closeBrowser()

