<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PER 40 boxscore button</name>
   <tag></tag>
   <elementGuidId>e278c9c7-1173-4075-b290-2f8469161742</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'current-season-filter-per40-id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>current-season-filter-per40-id</value>
   </webElementProperties>
</WebElementEntity>
