<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Presenter Mode (ScoutBuilder)</name>
   <tag></tag>
   <elementGuidId>e9dd32fc-0ca8-4eba-bef4-b8805ad0d17d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'startPresenterModeButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>startPresenterModeButton</value>
   </webElementProperties>
</WebElementEntity>
