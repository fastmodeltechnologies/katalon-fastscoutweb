<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Drop Target</name>
   <tag></tag>
   <elementGuidId>1b65d0ce-d0ed-42d7-b4b0-cd1e7b3a4a3a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = '//*[@id=&quot;left-arrow&quot;]']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>left-arrow</value>
   </webElementProperties>
</WebElementEntity>
