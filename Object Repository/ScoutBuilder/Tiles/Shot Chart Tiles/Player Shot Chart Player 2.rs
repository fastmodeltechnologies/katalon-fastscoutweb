<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>First player listed on Player Shot Chart tile</description>
   <name>Player Shot Chart Player 2</name>
   <tag></tag>
   <elementGuidId>0e5d1ceb-959f-4781-9743-a33e5a9c42d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;player-1&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;player-1&quot;]</value>
   </webElementProperties>
</WebElementEntity>
