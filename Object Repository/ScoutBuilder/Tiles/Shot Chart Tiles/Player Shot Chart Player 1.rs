<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>First player listed on Player Shot Chart tile</description>
   <name>Player Shot Chart Player 1</name>
   <tag></tag>
   <elementGuidId>1148c28d-5366-45a3-a8c0-50027bd60a93</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;player-0&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;player-0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
