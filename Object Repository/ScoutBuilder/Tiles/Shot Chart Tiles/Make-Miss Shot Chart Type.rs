<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Make-Miss Shot Chart Type</name>
   <tag></tag>
   <elementGuidId>00070d0f-e043-47f5-8a67-12fd2bc2ca9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'chart-2-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chart-2-label</value>
   </webElementProperties>
</WebElementEntity>
