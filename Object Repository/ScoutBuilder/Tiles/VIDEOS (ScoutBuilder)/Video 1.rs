<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Presence of first video on VIDEOS tab of a tile</description>
   <name>Video 1</name>
   <tag></tag>
   <elementGuidId>d549df0d-a186-4b07-9ebc-864989637e6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;asset-block-0&quot;]/div/div[1]/video</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;asset-block-0&quot;]/div/div[1]/video</value>
   </webElementProperties>
</WebElementEntity>
