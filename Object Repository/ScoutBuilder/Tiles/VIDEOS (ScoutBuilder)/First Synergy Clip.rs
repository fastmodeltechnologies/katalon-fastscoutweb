<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Fisrt listed Synergy clip after selecting Edit</description>
   <name>First Synergy Clip</name>
   <tag></tag>
   <elementGuidId>ae43a44b-cb35-4bf8-8d8c-e3cbe8345d40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;EditsTable&quot;]/tbody/tr[2]/td[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;EditsTable&quot;]/tbody/tr[2]/td[2]/div</value>
   </webElementProperties>
</WebElementEntity>
