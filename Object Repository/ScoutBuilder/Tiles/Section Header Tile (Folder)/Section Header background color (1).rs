<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>First option listed for the Section Header background color</description>
   <name>Section Header background color (1)</name>
   <tag></tag>
   <elementGuidId>be948763-4e5d-48de-99fa-4cd3f63f6698</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;sectionTitleStyle&quot;]/div/div[3]/span[1]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;sectionTitleStyle&quot;]/div/div[3]/span[1]/div</value>
   </webElementProperties>
</WebElementEntity>
