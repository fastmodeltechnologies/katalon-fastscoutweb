<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OPTIONS tab (FastScout Factors tile)</name>
   <tag></tag>
   <elementGuidId>a3c28ac9-b943-47ce-95f8-a1ac855086f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[2]</value>
   </webElementProperties>
</WebElementEntity>
