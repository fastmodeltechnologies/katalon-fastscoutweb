<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FastScout Factors Tile</name>
   <tag></tag>
   <elementGuidId>60e49e37-12bc-4369-aac6-14fc42e693dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;individualTileMenuOption-FastScout Factors&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;individualTileMenuOption-FastScout Factors&quot;]</value>
   </webElementProperties>
</WebElementEntity>
