<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>STATS tab (FastScout Factors tile)</name>
   <tag></tag>
   <elementGuidId>fe83feaf-3080-4435-993c-9bdb9f57420d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[3]</value>
   </webElementProperties>
</WebElementEntity>
