<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FTRate Checkbox (FastScout Factors)</name>
   <tag></tag>
   <elementGuidId>329eef61-4025-4496-a76d-78d356efc52a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;checkbox-FTRate-0&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;checkbox-FTRate-0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
