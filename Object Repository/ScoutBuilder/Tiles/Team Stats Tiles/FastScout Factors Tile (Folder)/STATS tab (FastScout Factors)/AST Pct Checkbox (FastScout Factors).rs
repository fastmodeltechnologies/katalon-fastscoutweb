<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AST Pct Checkbox (FastScout Factors)</name>
   <tag></tag>
   <elementGuidId>4d1f1e6a-3fc9-486d-bf63-46c3b6344216</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;checkbox-AST%-0&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;checkbox-AST%-0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
