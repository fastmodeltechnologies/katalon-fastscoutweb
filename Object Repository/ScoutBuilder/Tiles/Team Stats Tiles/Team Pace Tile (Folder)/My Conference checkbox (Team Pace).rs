<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>the My Conference checkbox found on the OPTIONS tab of the Team Pace tile</description>
   <name>My Conference checkbox (Team Pace)</name>
   <tag></tag>
   <elementGuidId>436ae35f-5b05-4d2e-bb26-640c87eae911</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'myConference-3-Pace']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>myConference-3-Pace</value>
   </webElementProperties>
</WebElementEntity>
