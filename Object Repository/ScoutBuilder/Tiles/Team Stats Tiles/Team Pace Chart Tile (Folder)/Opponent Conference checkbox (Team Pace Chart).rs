<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>the My Conference checkbox found on the OPTIONS tab of the Team Pace tile</description>
   <name>Opponent Conference checkbox (Team Pace Chart)</name>
   <tag></tag>
   <elementGuidId>53dc8827-32d7-4ada-ace0-6946c65bb8f2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'Pace-Chart-1-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Pace-Chart-1-label</value>
   </webElementProperties>
</WebElementEntity>
