<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Div containing section header tile to verify the tile is on the page</description>
   <name>Personnel Tile (Presense Of)</name>
   <tag></tag>
   <elementGuidId>ffd4ce22-7a04-4bc6-b6f4-3e18461c7c88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-tile-type = 'playerGroup']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-tile-type</name>
      <type>Main</type>
      <value>playerGroup</value>
   </webElementProperties>
</WebElementEntity>
