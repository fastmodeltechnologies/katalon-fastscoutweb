<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Div containing section header tile to verify the tile is on the page</description>
   <name>Leader Tile (Presense Of)</name>
   <tag></tag>
   <elementGuidId>976cfafa-fb35-479b-8456-6845ce76d193</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@data-tile-type = 'playerStatTable']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-tile-type</name>
      <type>Main</type>
      <value>playerStatTable</value>
   </webElementProperties>
</WebElementEntity>
