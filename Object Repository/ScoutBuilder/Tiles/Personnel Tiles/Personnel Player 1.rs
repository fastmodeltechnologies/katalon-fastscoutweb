<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Selects the first player listed on the PLAYERS tab for Personnel tile</description>
   <name>Personnel Player 1</name>
   <tag></tag>
   <elementGuidId>fec5856d-d24c-43a2-9d58-59570e4f6381</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;player-0-label&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;player-0-label&quot;]</value>
   </webElementProperties>
</WebElementEntity>
