<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Selects the first player listed on the PLAYERS tab for Personnel tile</description>
   <name>Personnel Player 2</name>
   <tag></tag>
   <elementGuidId>d1e8c79b-4eb5-4a4d-a3f1-7c465994cb2b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;player-1-label&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;player-1-label&quot;]</value>
   </webElementProperties>
</WebElementEntity>
