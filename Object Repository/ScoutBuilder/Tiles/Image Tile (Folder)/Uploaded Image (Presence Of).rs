<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Detects the presence of uploaded image on image tile</description>
   <name>Uploaded Image (Presence Of)</name>
   <tag></tag>
   <elementGuidId>3f977d26-fc72-40cb-a758-ffe26d94d26d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@id, 'image')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>image</value>
   </webElementProperties>
</WebElementEntity>
