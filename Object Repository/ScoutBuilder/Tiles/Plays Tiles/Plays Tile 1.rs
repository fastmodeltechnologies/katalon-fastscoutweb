<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1st play listed in Plays menu</description>
   <name>Plays Tile 1</name>
   <tag></tag>
   <elementGuidId>e6226f6b-b1b1-4000-8833-70826eb28ef2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;playsTableRows&quot;]/tbody/tr/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;playsTableRows&quot;]/tbody/tr/td[2]</value>
   </webElementProperties>
</WebElementEntity>
