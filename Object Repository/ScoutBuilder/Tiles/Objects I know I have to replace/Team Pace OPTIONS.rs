<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>OPTIONS tab for the Team Pace tile</description>
   <name>Team Pace OPTIONS</name>
   <tag></tag>
   <elementGuidId>4df33db2-53d0-4ecb-baed-1ea7d132975b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[2]</value>
   </webElementProperties>
</WebElementEntity>
