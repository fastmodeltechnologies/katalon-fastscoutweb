<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DONE button in Synergy Clips modal</name>
   <tag></tag>
   <elementGuidId>9b8663e8-117a-4fea-a112-97c1c6372731</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[17]/div/section/div[2]/div/header/h4/span/span/span[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[17]/div/section/div[2]/div/header/h4/span/span/span[2]/span</value>
   </webElementProperties>
</WebElementEntity>
