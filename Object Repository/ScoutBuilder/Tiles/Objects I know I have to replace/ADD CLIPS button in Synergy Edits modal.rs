<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ADD CLIPS button in Synergy Edits modal</name>
   <tag></tag>
   <elementGuidId>bbd8e478-75dd-4b0b-bc7b-0a6636e7d55b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[16]/div/section/div[2]/div/header/h4/span/span[2]/span[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[16]/div/section/div[2]/div/header/h4/span/span[2]/span[2]/span</value>
   </webElementProperties>
</WebElementEntity>
