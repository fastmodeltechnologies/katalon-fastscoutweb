<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PPP Overtime Checkbox</name>
   <tag></tag>
   <elementGuidId>becadac2-fd9e-483e-aa01-a8367fcf07aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[2]/div/div/div[1]/div/label/span/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[2]/div/div/div[1]/div/label/span/div/span</value>
   </webElementProperties>
</WebElementEntity>
