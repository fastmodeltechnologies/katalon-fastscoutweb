<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Custom split name</description>
   <name>SPLIT NAME text box</name>
   <tag></tag>
   <elementGuidId>5e78a358-88ad-4799-a1d6-31872f4ef68c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[47]/div/section/div[2]/div/div/div[2]/div/div[1]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[47]/div/section/div[2]/div/div/div[2]/div/div[1]/input</value>
   </webElementProperties>
</WebElementEntity>
