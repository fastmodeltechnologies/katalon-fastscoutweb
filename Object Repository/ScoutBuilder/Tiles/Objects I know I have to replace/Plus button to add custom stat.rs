<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Plus button to add custom stat</name>
   <tag></tag>
   <elementGuidId>9d995eb6-f027-4026-92cf-732242b45080</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div/div/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/div/svg</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div/div/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[1]/div[2]/div[2]/div/svg</value>
   </webElementProperties>
</WebElementEntity>
