<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Defense Tile Header Textbox (Input)</name>
   <tag></tag>
   <elementGuidId>f119f454-4768-4eac-bae5-0b82e1a30780</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[1]/div/div/div/div[1]/div/div/div/div/div/div[1]/div[6]/main/div/div/div/div/div/div/div[1]/div[2]/div/div/div/div[2]/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[1]/div/div/div/div[1]/div/div/div/div/div/div[1]/div[6]/main/div/div/div/div/div/div/div[1]/div[2]/div/div/div/div[2]/div[1]/div/input</value>
   </webElementProperties>
</WebElementEntity>
