<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lineup Stats Minimum Minutes</name>
   <tag></tag>
   <elementGuidId>ad0d1350-7129-45d7-a226-b548a6729b54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[4]/div[1]/div/label/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div/div[2]/div/div/div/div/div[1]/div/div[2]/div[2]/div/div[4]/div[1]/div/label/input</value>
   </webElementProperties>
</WebElementEntity>
