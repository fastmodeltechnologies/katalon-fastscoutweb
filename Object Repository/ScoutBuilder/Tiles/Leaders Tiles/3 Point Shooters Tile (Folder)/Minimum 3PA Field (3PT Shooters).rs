<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Minimum 3PA Field (3PT Shooters)</name>
   <tag></tag>
   <elementGuidId>6293c9a7-7864-4af1-b0ea-8a1199fc07d3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;Minimum-3PA&quot;]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;Minimum-3PA&quot;]/input</value>
   </webElementProperties>
</WebElementEntity>
