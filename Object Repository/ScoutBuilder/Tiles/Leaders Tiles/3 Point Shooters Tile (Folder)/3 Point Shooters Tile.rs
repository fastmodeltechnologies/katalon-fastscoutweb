<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>3 Point Shooters Tile</name>
   <tag></tag>
   <elementGuidId>b3517280-e876-4104-976f-afeaac93a7b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'individualTileMenuOption-3 Point Shooters']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>individualTileMenuOption-3 Point Shooters</value>
   </webElementProperties>
</WebElementEntity>
