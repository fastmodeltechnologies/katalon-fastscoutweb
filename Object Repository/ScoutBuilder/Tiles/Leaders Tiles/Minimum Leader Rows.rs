<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Minimum Leader Rows</name>
   <tag></tag>
   <elementGuidId>617f802e-4abd-4aba-a915-693958334cc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;topScorers#Rows&quot;]/div/label/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;topScorers#Rows&quot;]/div/label/input</value>
   </webElementProperties>
</WebElementEntity>
