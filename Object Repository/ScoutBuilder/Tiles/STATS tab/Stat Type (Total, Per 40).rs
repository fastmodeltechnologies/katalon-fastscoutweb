<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Stat Type (Total, Per 40)</name>
   <tag></tag>
   <elementGuidId>93333bbd-00e6-4a62-8d16-0e9e89c0280d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'boxscoreEditorStatType']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>boxscoreEditorStatType</value>
   </webElementProperties>
</WebElementEntity>
