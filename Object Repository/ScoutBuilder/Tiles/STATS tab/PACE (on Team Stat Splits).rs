<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PACE (on Team Stat Splits)</name>
   <tag></tag>
   <elementGuidId>775803ac-f14a-40f6-a7e3-38ef83e201e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'checkbox-POS-0-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkbox-POS-0-label</value>
   </webElementProperties>
</WebElementEntity>
