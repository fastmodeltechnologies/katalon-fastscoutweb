<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>PTS button in the Column order. Right-click to duplicate, click to toggle average/totals</description>
   <name>PTS Column Order</name>
   <tag></tag>
   <elementGuidId>6021a3f3-1fff-4146-b73a-168d659f167e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'col-item-PTS']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>col-item-PTS</value>
   </webElementProperties>
</WebElementEntity>
