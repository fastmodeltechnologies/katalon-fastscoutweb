<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Custom Stat Text Field</name>
   <tag></tag>
   <elementGuidId>c528d168-85b4-4a69-a027-f1631c267e7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@placeholder='Type custom stat']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@placeholder='Type custom stat']</value>
   </webElementProperties>
</WebElementEntity>
