<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Checkbox for rebound stat</description>
   <name>REB</name>
   <tag></tag>
   <elementGuidId>7a2252d2-46bc-4248-8457-8112fa967302</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'checkbox-Reb-0-label']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkbox-Reb-0-label</value>
   </webElementProperties>
</WebElementEntity>
