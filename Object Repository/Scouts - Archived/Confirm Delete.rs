<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Click delete to confirm deletion of report</description>
   <name>Confirm Delete</name>
   <tag></tag>
   <elementGuidId>ab908a3f-7068-4d26-ac28-905da300b8cf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[9]/div/section/div[2]/div/section/div/div[2]/div[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[9]/div/section/div[2]/div/section/div/div[2]/div[2]/a</value>
   </webElementProperties>
</WebElementEntity>
