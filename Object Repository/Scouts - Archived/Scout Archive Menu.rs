<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Scout-context menu on Archived tab</description>
   <name>Scout Archive Menu</name>
   <tag></tag>
   <elementGuidId>688e6a3c-666e-40b3-a272-974ec2192b41</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;archivedTable&quot;]/tbody/tr[2]/td[9]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;archivedTable&quot;]/tbody/tr[2]/td[9]/div</value>
   </webElementProperties>
</WebElementEntity>
