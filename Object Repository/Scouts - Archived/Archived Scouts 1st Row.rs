<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1st report listed in Archived Scouts tab</description>
   <name>Archived Scouts 1st Row</name>
   <tag></tag>
   <elementGuidId>b7fbd0d6-69c6-4047-8596-7f6a4eb03e27</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;archivedTable&quot;]/tbody/tr[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;archivedTable&quot;]/tbody/tr[2]</value>
   </webElementProperties>
</WebElementEntity>
