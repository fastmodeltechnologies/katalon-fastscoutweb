<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>ARCHIVED tab on SCOUTS page</description>
   <name>ARCHIVED tab</name>
   <tag></tag>
   <elementGuidId>6f2b35fd-0318-4383-9813-cd3c89a5929c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;archived-4&quot;]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;archived-4&quot;]/span</value>
   </webElementProperties>
</WebElementEntity>
