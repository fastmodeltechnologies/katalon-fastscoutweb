<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>First scout in report list on SCOUTS>OPPONENT SCOUTS page</description>
   <name>Scout Content Menu (Opponent Scouts)</name>
   <tag></tag>
   <elementGuidId>cea8c551-645e-43c4-a72b-44d126c5a90c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]/td[10]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;reportsTable&quot;]/tbody/tr[2]/td[10]/div</value>
   </webElementProperties>
</WebElementEntity>
