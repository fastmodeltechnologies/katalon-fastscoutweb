<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Archive button</description>
   <name>Archive</name>
   <tag></tag>
   <elementGuidId>3fd714f2-2378-4424-b6cd-f3b57c1b40a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'archive']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>archive</value>
   </webElementProperties>
</WebElementEntity>
