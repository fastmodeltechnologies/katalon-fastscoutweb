<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Saved Template field to select a saved template for a scouting report</description>
   <name>Saved Template Dropdown</name>
   <tag></tag>
   <elementGuidId>dd844fba-b9c9-4306-b190-a696be9d75eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;choosesavedTemplateDropdown&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;choosesavedTemplateDropdown&quot;]</value>
   </webElementProperties>
</WebElementEntity>
