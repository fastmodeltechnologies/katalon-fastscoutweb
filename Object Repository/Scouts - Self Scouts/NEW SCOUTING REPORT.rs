<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>NEW SCOUTING REPORT button</description>
   <name>NEW SCOUTING REPORT</name>
   <tag></tag>
   <elementGuidId>55478f58-4381-4d7d-b5f4-f0f07238f8be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div[1]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[1]/div/div/div/div/div[1]/div/span</value>
   </webElementProperties>
</WebElementEntity>
