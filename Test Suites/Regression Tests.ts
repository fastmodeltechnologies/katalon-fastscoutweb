<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>FastScout Web Regression Tests</description>
   <name>Regression Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f75df59f-307f-407e-8e50-26c2c9121b13</testSuiteGuid>
   <testCaseLink>
      <guid>b5bb7991-4943-46d8-9184-1592c25b0da3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC1 - New Report - Template 1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0709bd70-95a8-4c25-a4bb-35edb9901e5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC2 - New Report - Template 2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba64af75-7753-4a0d-8f8b-9d9c652220da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC3 - New Report - Template 3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c50aeeb6-9753-4534-9d85-282dea263fed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC4 - New Report - Template 4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f50f0d0c-9c0a-439e-b6a9-3e158b217315</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TC5 - New Report - Template 5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>328bd846-d025-4c56-8715-f241cf396914</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC6 - New Report - Template 6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7627ac28-0a82-4778-a429-86bb0bea8e14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC7- New Report - Add All Tiles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a242291d-afd9-40ea-a769-fc56d7cb0031</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC8 - Edit All Tiles</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
